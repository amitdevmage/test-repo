
/*jshint browser:true */
define([
    'jquery',
    'ko',
    'Magestore_BarcodeSuccess/js/full-screen-loader',
    'Magestore_BarcodeSuccess/js/alert',
    'mage/translate'
], function ($, ko, fullScreenLoader, Alert, Translate) {
    'use strict';
    $.widget('mage.hsBarcodeScan', {
        /**
         * Create widget
         * @private
         */
        _create: function () {
            this.detailContainer = this.element.find('#detailContainer');
            this.scanInput = this.element.find('#os_barcode_scan');
            this.actionsEls = this.element.find('[data-role=action]');
            this.dataEls = this.element.find('[data-role=data]');
            this.hasResult = ko.observable(false);
            this.result = ko.observable();
            this.template = ko.observable();
            this.qty = ko.observable();
            this._bindEvent();
        },
        _bindEvent: function () {
            var self = this;
           
            if(self.scanInput){
                self.scanInput.change(function(event){
                    self.scanBarcode(event.target.value);
                });
            }
        },
        processResponse: function(response){
            var self = this;
            if(response.success && response.data){
                if(self.dataEls && self.dataEls.length > 0){
                    self.result(response.data);
                    $.each(self.dataEls,function(){
                        var key = this.getAttribute('data-key');
                        var type = this.getAttribute('data-type');
                        if(response.data[key]){
                            if(type == 'text'){
                                this.innerHTML = response.data[key];
                            }
                            if(type == 'image'){
                                this.setAttribute('src', response.data[key]);
                            }
                        }
                    });
                }
            }
            if(response.error && response.messages){
                Alert('Error',response.messages);
            }
            if(response.redirect){
                window.location = response.redirect;
            }
        },
        scanBarcode: function(barcode){
            var self = this;
            if(!barcode){
                return false;
            }
          //  fullScreenLoader.startLoader();
            var barcode = barcode.trim();
            var params = {
                barcode:barcode
            };
            $.ajax({
                url: self.options.barcodeurl,
                data:params,
                success: function(result){
                    self.scanInput.val("");
                    print_r(result.data);
                  //  fullScreenLoader.stopLoader();
                   // self.processResponse(result);
                },
                error: function(error){
                    self.scanInput.val("");
                  //  fullScreenLoader.stopLoader();
                }
            });
        }
    });

    return $.mage.hsBarcodeScan;
});
