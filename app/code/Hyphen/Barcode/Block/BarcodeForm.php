<?php

 
namespace Hyphen\Barcode\Block;

class BarcodeForm extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('barcode/index/save', ['_secure' => true]);
    }

    public function getBarcodeInitData(){
        $data = [];
        $data['barcodeurl'] = $this->getUrl('barcode/index/post', ['_secure' => true]);
       
        return \Zend_Json::encode($data);
    }

}
