<?php


namespace Hyphen\Barcode\Controller\Index;
use \Magento\Framework\Controller\Result\JsonFactory;
use Magento\Catalog\Model\ProductFactory;

class Post extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $productFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        JsonFactory $resultJsonFactory,
        ProductFactory $productFactory
    ) {
        $this->productFactory = $productFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $barcode = $this->getRequest()->getParam('barcode');
        //echo '<pre>'; echo 'hello'; print_r($barcode); die;
        if(empty($barcode)){
            $resultJson->setData([
                'messages' => __('Please scan your barcode to get data'),
                'error' => true
            ]);
        } else {
            $data = [];
            $product = $this->loadProductByBarCode($barcode);
          
            if ($product) {
                    
                  
                        $id = $product->getId();
                       
                       
                        $data['product_name'] = $product->getName();
                        $data['product_price'] = $product->getPrice();
                        $data['product_weight'] = $product->getWeight();
                        $data['product_color'] = $product->getColor();
                        $data['id'] = $product->getId();
                      
                        
                        $data['product_status'] = ($product->getStatus())?__('Enabled'):__('Disabled');
                        
                   
               
                
                $resultJson->setData([
                    'data' => $data,
                    'success' => true
                ]);
            }else{
                $resultJson->setData([
                    'messages' => __('Cannot find any records for %1', $barcode),
                    'error' => true
                ]);
            }
        }

        return $resultJson;

    }

    public function loadProductByBarCode($barcode)
    {
        return $this->productFactory->create()->loadByAttribute('barcode',$barcode);
    }
}
