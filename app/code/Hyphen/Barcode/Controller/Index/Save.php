<?php


namespace Hyphen\Barcode\Controller\Index;
use \Magento\Framework\Controller\Result\JsonFactory;
use Magento\Catalog\Model\ProductFactory;

class Save extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $productFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        JsonFactory $resultJsonFactory,
        ProductFactory $productFactory
    ) {
        $this->productFactory = $productFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
       // echo 'Hello'; die;
        $product_id = $this->getRequest()->getParam('product_id');
        $product_name = $this->getRequest()->getParam('product_name');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load(1);
        $product->setName('teststts'); // Name of Product
        $product->save();
        return 1;
        
    }

   }
